# Projeto XXXX
### Especificações de Requisito de Software
Versão 1.0 - 22/08/2017

|Versão|Autor(es)                            |Data      |Ação                          |
|------|-------------------------------------|----------|------------------------------|
|1.0   |Rodrigo Chaves|30/01/2017|Alteração Ocorrida|

## Conteúdo
1 Introdução <br />
1.1 Objetivo <br />
1.2 Escopo <br />
1.3 Definições, acrônimos e abreviações <br />
1.4 Referencias <br />
1.5 Objetivo <br />
<br />
2 Descrição Geral <br />
<br />
3 Requisitos de Software <br />
3.1 Requisitos funcionais <br />
3.2 Requisitos não Funcionais <br />

## 1. Introdução
Deve incluir a finalidade, o escopo, as definições, os acrônimos, as abreviações, as referências e a visão geral

### 1.1. Objetivo
Deverá descrever totalmente o comportamento externo do aplicativo ou do subsistema identificado. Ela também deverá descrever requisitos não funcionais, restrições de design e outros fatores necessários para fornecer uma visão completa e abrangente dos requisitos do software.

### 1.2. Escopo
Descrever o recurso ou outro agrupamento de subsistemas; a que modelo(s) de Caso de Uso a SRS está associada; e tudo o mais que seja afetado ou influenciado por este documento.

### 1.3. Definições, Acrônimos e Abreviações
Esta subseção deve fornecer as definições de todos os termos, acrônimos e abreviações necessárias à adequada interpretação da documentação.

### 1.4. Referências
Esta subseção deve fornecer uma lista completa de todos os documentos mencionados em qualquer outra parte da documentação.
(Texto adaptado de [url](http://www.funpar.ufpr.br:8080/rup/webtmpl/templates/req/rup_srs.htm)

### 1.5. Visão Geral
Esta subseção descreve o que o restante da documentação contém e explica como o documento está organizado.

## 2. Descrição geral
Esta seção da documentação deve descrever os fatores gerais que afetam o produto e seus requisitos. Ela não deve especificar requisitos específicos. Em vez disso, deve fornecer uma base para esses requisitos, que serão definidos detalhadamente na Seção 3, e facilitar sua compreensão. Inclua itens como:
- perspectiva do produto
- funções do produto
- características do usuário
- restrições
- suposições e dependências
- subconjuntos de requisitos

## 3. Requisitos de software 
Esta seção da documentação deve conter todos os requisitos de software em um nível de detalhamento suficiente para possibilitar que os designers projetem um sistema que satisfaça esses requisitos e que os testadores verifiquem se o sistema satisfaz esses requisitos.   Quando for utilizada a modelagem de casos de uso, esses requisitos serão capturados nos Casos de Uso e nas especificações suplementares aplicáveis. Se a modelagem de casos de uso não for utilizada, o esquema das especificações suplementares poderá ser inserido diretamente nesta seção, conforme mostrado a seguir.

### 3.1. Requisitos funcionais
Esta seção descreve os requisitos funcionais do sistema que são expressos no estilo de linguagem natural. Para muitos aplicativos, este poderá ser o volume do Pacote da documentação. É necessário refletir muito para organizar esta seção. Normalmente, esta seção é organizada por recurso, mas outros métodos de organização também poderão ser adequados como, por exemplo, a organização por usuário ou a organização por subsistema. Entre os requisitos funcionais poderão estar incluídos conjuntos de recursos, capacidades e segurança.

#### 3.1.1. Requisito funcional 1
Descrição do requisito 1.

#### 3.1.2. Requisito funcional 2
Descrição do requisito 2.

### 3.2. Requisitos não funcionais
Descrição de tecnologias utilizadas, como citado nos exemplo abaixo
#### 3.2.1. Requisito não funcional 1: Especificação de projeto
Código em linguagem Java e especificação de projeto baseada em UML2.

#### 3.2.2. Requisito não funcional 2: Interface gráfica para usuário
O software deverá ter interface gráfica única, partilhada pelos usuários.